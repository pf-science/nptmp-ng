use std::io::prelude::*;
use std::io::SeekFrom;
use std::fs::File;

fn isqrt(x: u32) -> u32 {
    (x as f64).sqrt() as u32
}

fn dfx(n: u32) -> u32 {
    let mut x: u32 = n;
    let mut count: u32 = 1;
    let mut sqr: u32 = isqrt(x);
    let mut i: u32 = 2;

    loop {
        let mut pow: u32 = 0;

        while x % i == 0 {
            pow += 1;
            x /= i;
        }

        if pow != 0 {
            count *= pow + 1;
            sqr = isqrt(x)
        }

        if i == 2 {
            i = 3;
        } else {
            i += 2;
        }

        if i > sqr {
            break;
        }
    }

    if x != 1 {
        count *= 2;
    }

    return count;
}

fn main() -> std::io::Result<()>{
    let mut res = File::create("result")?;
    let mut chkp = File::create("checkpoint")?;

    for x in 1..100 {
        write!(res, "{}\t{}\n", x, dfx(x))?;
        res.flush()?;

        chkp.seek(SeekFrom::Start(0))?;
        write!(chkp, "{}\n", x)?;
        chkp.flush()?;
    }

    Ok(())
}
